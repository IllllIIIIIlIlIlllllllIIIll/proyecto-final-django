# Generated by Django 3.0.4 on 2020-06-26 11:25

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='File_Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file_name', models.CharField(max_length=200)),
                ('file_description', models.TextField()),
                ('file_post', models.FileField(blank=True, upload_to='Files')),
            ],
        ),
        migrations.CreateModel(
            name='Hilo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('imagen_name', models.CharField(max_length=200)),
                ('imagen_description', models.TextField()),
                ('imagen_file', models.ImageField(blank=True, upload_to='Image')),
            ],
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('title', models.CharField(max_length=150)),
                ('imagen', models.ImageField(blank=True, upload_to='Image_post')),
                ('text', models.TextField()),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('file_in', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='sir.File_Post')),
                ('hilo', models.ManyToManyField(to='sir.Hilo')),
            ],
        ),
        migrations.CreateModel(
            name='forum',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hilo_in_forum', models.ManyToManyField(to='sir.Hilo')),
                ('post_in_forum', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='sir.Post')),
            ],
        ),
    ]
