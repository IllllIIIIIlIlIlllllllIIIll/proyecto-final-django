from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from sir import views

app_name = 'Sir'

urlpatterns = [

    path('Add_Post' , views.Add_Post , name="Add_Post" ),
    path('Edit_Post/<int:pk>' , views.Edit_Post , name="Edit_Post" ),
    path('View_Post/<int:pk>' , views.View_Post , name="View_Post" ),
    path('Index_post' , views.Index_post , name="Index_post" ),
    path('Delete_Post/<int:pk>', views.Delete_Post, name='Delete_Post'),

    path('Add_Forums' , views.Add_Forums , name="Add_Forums" ),
    path('View_Forums/<int:pk>' , views.View_Forums , name="View_Forums" ),
    path('Index_Forums' , views.Index_Forums , name="Index_Forums" ),
    path('Index_Forums_Owner' , views.Index_Forums_Owner , name="Index_Forums_Owner" ),
    path('Edit_File/<int:pk>' , views.Edit_File , name="Edit_File" ),

]