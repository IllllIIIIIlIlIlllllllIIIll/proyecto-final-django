from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(File_Post)
admin.site.register(Hilo)
admin.site.register(Post)
admin.site.register(Forum)
