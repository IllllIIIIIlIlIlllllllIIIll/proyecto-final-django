from django.shortcuts import render , redirect, get_object_or_404
from django.urls import reverse
from django.http import HttpResponse
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login as do_login
from django.contrib.auth import logout as do_logout
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import SetPasswordForm , AuthenticationForm , UserChangeForm , UserCreationForm
from django.contrib.auth.models import User,Group
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import updateForm

def login(request):
    
    form = AuthenticationForm()
    if request.method == "POST":
        
        form = AuthenticationForm(data=request.POST)
        
        if form.is_valid():
            
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

          
            user = authenticate(username=username, password=password)

            if user is not None:
               
                do_login(request, user)
                messages.success(request, 'se ha iniciado sessión correctamente')
                return redirect('Sir:Index_Forums')
    return render(request, "acciones/login.html", {'form': form})

def register(request):
    
    if request.user.is_authenticated:
        return redirect('Usuarios:Profile')

    form = UserCreationForm(request.POST or None)

    if request.method == 'POST' and form.is_valid():       
        usera = form.save()
        group = Group.objects.get(name='lector')
        usera.groups.add(group)
        if usera:
            messages.success(request, 'Usuario Creado Exitosamente')
            print("usuario Creado")
            return redirect('Usuarios:login')
        else:
            print("usuario no creado")
            messages.error(request, 'Algunos de los campos no están correctos. Por favor, verifique los datos y vuelva a intentarlo.')
    return render(request,'acciones/registrar.html',{'form':form})

@login_required
def logout(request):
    do_logout(request)
    messages.success(request, 'Tu sessión se ha cerrado correctamente!')
    return redirect('Usuarios:login')

@login_required
def ChangePassword(request):
    print ("wenardo")

    usir = User.objects.get(username=request.user.username)
    print (usir.id)
    print(request.method)
    if request.method == 'POST':
        form = SetPasswordForm(usir, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  
            messages.success(request, 'Tu contraseña se actualizó correctamente!')
            return redirect('Usuarios:Profile')
        else:
            messages.error(request, 'Uuuy tenemos un error.')
    else:
        form = SetPasswordForm(usir)
    return render(request, 'acciones/cambio_pass.html', {
        'form': form
    })

@login_required
def editUser(request):

    form = updateForm (instance = request.user)
    if request.method == 'POST':
        form = updateForm(data=request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            messages.success(request, 'Tus datos se han actualizado correctamente!')
            return redirect('Usuarios:Profile')
        else:
            messages.error(request, 'Uuuy tenemos un error.')
            
    return render(request,'acciones/editUser.html',{'form':form})

@login_required
def Profile(request):
    if request.user.is_authenticated:
        template = 'acciones/miperfil.html'
        userrr = get_object_or_404(User, username=request.user.username)
        context = {"Userr": userrr}
        return render(request, template, context)

    else:    
        return render(request,'acciones/miperfil.html',)

