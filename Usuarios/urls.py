from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from Usuarios import views

app_name = 'Usuarios'

urlpatterns = [

    path('register' , views.register , name="register" ),
    path('login', views.login, name="login"),
    path('logout', views.logout ,name="logout"),
    path('ChangePassword', views.ChangePassword ,name="ChangePassword"),
    path('editUser',views.editUser,name="editUser"),
    path('Profile' , views.Profile , name="Profile" ),

    


]
