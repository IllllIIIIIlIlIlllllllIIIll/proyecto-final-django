from django import forms
from django.forms import widgets

from .models import *

Job_Type_Job = (
    ('Inspeccion', 'Inspeccion'),
    ('Modificacion', 'Modificacion'),
    ('Correctivo', 'Correctivo'),
    ('Preventivo', 'Preventivo'),
    ('Instalacion', 'Instalacion'),
    ('Desinstalacion', 'Desinstalacion'),
)

Job_Type_Contract = (
    ('Garantia', 'Garantia'),
    ('Contrato', 'Contrato'),
    ('Sin covertura', 'Sin covertura'),
)

Inform_choices = ( 
    (None, 'Seleccione una opcion...'), 
    (False, 'Rechasado'), 
    (True, 'Aprobado'), 

) 
class DateInput(forms.DateTimeInput):
    input_type='datetime'

class NewDocument(forms.Form):

    name = forms.CharField( label='Nombres',  required=True , widget=forms.TextInput ( attrs={'class':'form-control' , 'id':'name' , 'placeholder':'Nombres'} ))
    rut = forms.CharField(label='Rut',  required=True , widget=forms.TextInput ( attrs={'class':'form-control' , 'id':'run' , 'placeholder':'12345678-9'} ))
    direction = forms.CharField( label='Dirección',  required=True , widget=forms.TextInput ( attrs={'class':'form-control' , 'id':'direction' , 'placeholder':'Dirección'} ))
    phone = forms.CharField( label='Nombres',  required=True , widget=forms.TextInput ( attrs={'class':'form-control' , 'id':'phone' , 'placeholder':'phone'} ))
    email = forms.EmailField( required=True , widget = forms.EmailInput ( attrs = {'class':'form-control' , 'id':'email' , 'placeholder':'Usuario@server.com'} ))

    dateHour  = forms.DateTimeField(label='Fecha Solicitada', widget = DateInput( attrs={'class':'form-control' , 'id':'dateHour' } ))
 
    applicant = forms.CharField(label='Solicita',  required=True , widget=forms.TextInput ( attrs={'class':'form-control' , 'id':'first_name' , 'placeholder':'Nombres'} ))
    attended = forms.CharField(label='Atencion',  required=True , widget=forms.TextInput ( attrs={'class':'form-control' , 'id':'first_name' , 'placeholder':'Nombres'} ))
    makedFor = forms.CharField(label='Realizado por',  required=True , widget=forms.TextInput ( attrs={'class':'form-control' , 'id':'first_name' , 'placeholder':'Nombres'} ))
    reception = forms.CharField(label='Recepcion',  required=True , widget=forms.TextInput ( attrs={'class':'form-control' , 'id':'first_name' , 'placeholder':'Nombres'} ))

    system = forms.CharField(label='Sistema',  required=True , widget=forms.TextInput ( attrs={'class':'form-control' , 'id':'first_name' , 'placeholder':'Nombres'} ))
    mark = forms.CharField(label='Marca',  required=True , widget=forms.TextInput ( attrs={'class':'form-control' , 'id':'first_name' , 'placeholder':'Nombres'} ))
    model = forms.CharField(label='Modelo',  required=True , widget=forms.TextInput ( attrs={'class':'form-control' , 'id':'first_name' , 'placeholder':'Nombres'} ))
    serie = forms.CharField(label='Serie',  required=True , widget=forms.TextInput ( attrs={'class':'form-control' , 'id':'first_name' , 'placeholder':'Nombres'} ))

    securityCheck = forms.TypedChoiceField(choices=Inform_choices)
    generalPerformance = forms.TypedChoiceField(choices=Inform_choices)
    serviceandCalibration = forms.TypedChoiceField(choices=Inform_choices)
    digitalEquipment = forms.TypedChoiceField(choices=Inform_choices)
    systemOperation = forms.TypedChoiceField(choices=Inform_choices)


    Job_Type_Job = forms.TypedChoiceField(choices=Job_Type_Job)
    Job_Type_Contract = forms.TypedChoiceField(choices=Job_Type_Contract)
    Observation = forms.CharField(label='Observación',  required=True , widget=forms.Textarea ( attrs={'class':'form-control' , 'id':'observation' , 'placeholder':'observacion'} ))

    def save(self):
        ClientData = Client(name=self.cleaned_data.get('name') ,rut=self.cleaned_data.get('rut') ,direction=self.cleaned_data.get('direction') ,phone= self.cleaned_data.get('phone'),email= self.cleaned_data.get('email'))
        ClientData.save()
        DateData = DateOn(dateHour=self.cleaned_data.get('dateHour') , applicant=self.cleaned_data.get('applicant') , attended=self.cleaned_data.get('attended') , makedFor= self.cleaned_data.get('makedFor'), reception=self.cleaned_data.get('reception') )
        DateData.save()
        equipmentData = Device(system=self.cleaned_data.get('system') , mark=self.cleaned_data.get('mark') , model=self.cleaned_data.get('model') , serie= self.cleaned_data.get('serie'))
        equipmentData.save()
        InformData = Inform(securityCheck=self.cleaned_data.get('securityCheck') , generalPerformance = self.cleaned_data.get('generalPerformance') , serviceandCalibration=self.cleaned_data.get('serviceandCalibration') , digitalEquipment=self.cleaned_data.get('digitalEquipment') , systemOperation=self.cleaned_data.get('systemOperation') )
        InformData.save()
        DocumentData = Document(Client=ClientData , DateOn=DateData , Inform =InformData ,Device = equipmentData ,Job_Type_Job=self.cleaned_data.get('Job_Type_Job') , Job_Type_Contract= self.cleaned_data.get('Job_Type_Contract'), Observation= self.cleaned_data.get('Observation'))
        DocumentData.save()

"""

class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = '__all__'


class DateOnForm(forms.ModelForm):
    class Meta:
        model = DateOn
        fields = '__all__'

class DeviceForm(forms.ModelForm):
    class Meta:
        model = Device
        fields = '__all__'

Job_Type_Job = (
    ('Inspeccion', 'Inspeccion'),
    ('Modificacion', 'Modificacion'),
    ('Correctivo', 'Correctivo'),
    ('Preventivo', 'Preventivo'),
    ('Instalacion', 'Instalacion'),
    ('Desinstalacion', 'Desinstalacion'),
)

Job_Type_Contract = (
    ('Garantia', 'Garantia'),
    ('Contrato', 'Contrato'),
)

class InformForm(forms.ModelForm):
    class Meta:
        model = Inform
        fields = '__all__'

class documentForm(forms.ModelForm):
    class Meta:
        model = document
        fields = '__all__'
"""
