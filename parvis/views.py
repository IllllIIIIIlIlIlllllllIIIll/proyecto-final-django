from django.shortcuts import render , redirect, get_object_or_404
from django.urls import reverse
from django.views.generic import CreateView
from .models import*
from .forms import *

# Create your views here.
def DocumentList(request):
    template = 'acciones/IndexDocument.html'
    document = Document.objects.all()
    context = {"document": document}
    return render(request, template, context)

def DocumentCreate(request):
    form= NewDocument(request.POST,request.FILES or None)
    template = 'acciones/form.html'
    if form.is_valid():
        form.save()
        return redirect('Parvis:IndexDocument')
    context = {"form": form}
    return render(request, template, context)

def DocumentEdit(request, pk):
    template = 'acciones/form.html'
    document =get_object_or_404(Document, pk=pk)
    form= NewDocument(request.POST or None, instance = document)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return redirect('Parvis:IndexDocument')
    context = {"form": form}
    return render(request, template, context)


def DocumentView(request, pk):
    template = 'acciones/lookDocument.html'
    document =get_object_or_404(Document, pk=pk)
    print(document.Client.name)
    context = {"document": document}
    return render(request, template, context)

