from django.db import models
from django.contrib.auth.models import User
from datetime import datetime  
from django.utils import timezone

# Create your models here.

class Client(models.Model):
    name = models.CharField(max_length=150)
    rut = models.CharField(max_length=150)
    direction = models.CharField(max_length=150)
    phone = models.CharField(max_length=150)
    email = models.EmailField()


class DateOn(models.Model):
    dateHour  = models.DateTimeField()
    applicant = models.CharField(max_length=150)
    attended = models.CharField(max_length=150)
    makedFor = models.CharField(max_length=150)
    reception = models.CharField(max_length=150)


class Device(models.Model):
    system = models.CharField(max_length=150)
    mark = models.CharField(max_length=150)
    model = models.CharField(max_length=150)
    serie = models.CharField(max_length=150)

Job_Type_Job = (
    ('Inspeccion', 'Inspeccion'),
    ('Modificacion', 'Modificacion'),
    ('Correctivo', 'Correctivo'),
    ('Preventivo', 'Preventivo'),
    ('Instalacion', 'Instalacion'),
    ('Desinstalacion', 'Desinstalacion'),
)

Job_Type_Contract = (
    ('Garantia', 'Garantia'),
    ('Contrato', 'Contrato'),
    ('Sin covertura', 'Sin covertura'),
)

class Inform(models.Model):
    securityCheck = models.BooleanField()
    generalPerformance = models.BooleanField()
    serviceandCalibration = models.BooleanField()
    digitalEquipment = models.BooleanField()
    systemOperation = models.BooleanField()

class Document(models.Model):
    Client = models.OneToOneField(Client,default=None, on_delete=models.CASCADE)
    DateOn = models.OneToOneField(DateOn,default=None, on_delete=models.CASCADE)
    Device = models.OneToOneField(Device,default=None, on_delete=models.CASCADE)
    Job_Type_Job = models.CharField(choices=Job_Type_Job, max_length=16)
    Job_Type_Contract = models.CharField(choices=Job_Type_Contract , max_length=16)
    Inform = models.OneToOneField(Inform,default=None, on_delete=models.CASCADE)
    Observation = models.TextField()



