from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from parvis import views

app_name = 'Parvis'

urlpatterns = [

    path('CreateDocument' , views.DocumentCreate , name="CreateDocument" ),
    path('EditDocument/<int:pk>' , views.DocumentEdit , name="EditDocument" ),
    path('DocumentView/<int:pk>' , views.DocumentView , name="DocumentView" ),
    path('IndexDocument' , views.DocumentList , name="IndexDocument" ),


    


]