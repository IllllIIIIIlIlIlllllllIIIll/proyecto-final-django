from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Client)
admin.site.register(DateOn)
admin.site.register(Device)
admin.site.register(Inform)
admin.site.register(Document)